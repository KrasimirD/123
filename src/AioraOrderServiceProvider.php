<?php
namespace KrasimirD\123;

use KrasimirD\123\Commands\UpdateSwaggerAnnotationConfig;
use KrasimirD\123\Providers\EventServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

class AioraOrderServiceProvider extends  ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/aiora-order-service.php' => config_path("order-service.php"),
        ],"aiora-config");

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $kernel = $this->app->make(Kernel::class);

        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    UpdateSwaggerAnnotationConfig::class
                ]);
        }

    }

    public function register()
    {
        $this->app->register(EventServiceProvider::class);
    }
}
